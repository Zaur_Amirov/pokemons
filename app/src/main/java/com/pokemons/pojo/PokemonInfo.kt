package com.pokemons.pojo

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "pokemons")
data class PokemonInfo(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String? = null,
    @SerializedName("weight")
    @Expose
    val weight: Int? = null,
    @SerializedName("height")
    @Expose
    val height: Int? = null,
    @SerializedName("base_experience")
    @Expose
    val baseExperience: Int? = null,
    @Embedded
    @SerializedName("sprites")
    @Expose
    val sprites: Sprites? = null
)