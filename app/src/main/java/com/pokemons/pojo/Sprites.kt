package com.pokemons.pojo

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "sprites")
data class Sprites(
    @SerializedName("back_default")
    @Expose
    val backDefault: String? = null,
    @SerializedName("back_shiny")
    @Expose
    val backShiny: String? = null,
    @SerializedName("front_default")
    @Expose
    val frontDefault: String? = null,
    @SerializedName("front_shiny")
    @Expose
    val frontShiny: String? = null
)