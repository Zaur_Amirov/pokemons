package com.pokemons.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Pokemons(
    @SerializedName("count")
    @Expose
    val count: Int? = null,
    @SerializedName("next")
    @Expose
    val next: String? = null,
    @SerializedName("previous")
    @Expose
    val previous: String? = null,
    @SerializedName("results")
    @Expose
    val results: List<Pokemon>? = null
)