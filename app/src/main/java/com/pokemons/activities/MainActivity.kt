package com.pokemons.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pokemons.fragments.MainFragment
import com.pokemons.fragments.OnBackChildFragmentEventListener
import com.pokemons.fragments.PokemonDetailsFragment
import com.pokemons.R
import com.pokemons.adapters.PokemonAdapter
import com.pokemons.pojo.PokemonInfo

class MainActivity : AppCompatActivity(), PokemonAdapter.OnPokemonClickListener,
    OnBackChildFragmentEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fTrans = supportFragmentManager.beginTransaction()
        fTrans.add(
            R.id.activity_main_container,
            MainFragment.newInstance()
        )
        fTrans.commit()
    }

    open override fun onPokemonClick(pokemonInfo: PokemonInfo) {
        val fTrans = supportFragmentManager.beginTransaction()
        fTrans.replace(
            R.id.activity_main_container,
            PokemonDetailsFragment.newInstance(pokemonInfo)
        )
        fTrans.commit()
    }

    override fun onBackChildFragment() {
        val fTrans = supportFragmentManager.beginTransaction()
        fTrans.replace(
            R.id.activity_main_container,
            MainFragment.newInstance()
        )
        fTrans.commit()
    }
}