package com.pokemons.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pokemons.R
import com.pokemons.pojo.PokemonInfo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonAdapter(private val context: Context) :
    RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder>() {

    var onPokemonClickListener: OnPokemonClickListener? = null

    var pokemonList: List<PokemonInfo> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivLogoPokemon = itemView.ivLogoPokemon
        val tvNamePokemon = itemView.tvNamePokemon
        val tvBaseExperiencePokemon = itemView.tvBaseExperiencePokemon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return PokemonViewHolder(view)
    }

    override fun getItemCount() = pokemonList.size

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val coin = pokemonList[position]
        with(holder) {
            with(coin) {
                val nameTemplate = context.resources.getString(R.string.name_template)
                val baseExperienceTemplate = context.resources.getString(R.string.base_experience_template)
                tvNamePokemon.text = String.format(nameTemplate, name)
                tvBaseExperiencePokemon.text = String.format(baseExperienceTemplate, baseExperience)
                Picasso.get().load(sprites?.frontShiny).into(ivLogoPokemon)
                itemView.setOnClickListener{
                    onPokemonClickListener?.onPokemonClick(this)
                }
            }
        }
    }

    interface OnPokemonClickListener{
        fun onPokemonClick(pokemonInfo: PokemonInfo)
    }
}