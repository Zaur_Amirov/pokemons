package com.pokemons.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pokemons.pojo.PokemonInfo

@Dao
interface PokemonDao {

    @Query("SELECT * FROM pokemons")
    fun getPokemonList(): LiveData<List<PokemonInfo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemon(pokemonList: List<PokemonInfo>)
}