package com.pokemons.api

import com.pokemons.pojo.PokemonInfo
import com.pokemons.pojo.Pokemons
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiService {

    @GET("pokemon")
    fun getPokemons(
    ): Single<Pokemons>

    @GET()
    fun getPokemonsNext(@Url url: String): Single<Pokemons>

    @GET()
    fun getPokemon(@Url url: String): Single<PokemonInfo>

}