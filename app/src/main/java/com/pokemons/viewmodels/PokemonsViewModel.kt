package com.pokemons.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pokemons.api.ApiFactory
import com.pokemons.database.AppDatabase
import com.pokemons.pojo.PokemonInfo
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class PokemonsViewModel(application: Application) : AndroidViewModel(application) {

    private val db = AppDatabase.getInstance(application)
    private val compositeDisposable = CompositeDisposable()

    var getPokemonList = db.pokemonDao().getPokemonList()

    init {
        //AppDatabase.getInstance(application)
        loadData()
    }

    private fun loadData() {
        val disposable = ApiFactory.apiService.getPokemons()
            .map {
                if(it.next != null) loadNext(it.next)
                it.results?.map { it1 ->
                    it1.url?.let { it2 ->
                        it2
                        ApiFactory.apiService.getPokemon(it2)
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it != null) {
                    var pokemonsList = ArrayList<PokemonInfo>()
                    for (pokemon in it) {
                        val subscribe = pokemon?.subscribe(Consumer { it1 ->
                            it1
                            pokemonsList.add(it1)
                        })
                    }
                    db.pokemonDao().insertPokemon(pokemonsList)
                }
            }, {
                Log.d("TEST_OF_LOADING_DATA", "Failure: " + it.message)
            })
        compositeDisposable.add(disposable)
    }

    private fun loadNext(nextRequest: String) {
        val disposable = ApiFactory.apiService.getPokemonsNext(nextRequest)
            .map {
                if(it.next != null) loadNext(it.next)
                it.results?.map { it1 ->
                    it1.url?.let { it2 ->
                        it2
                        ApiFactory.apiService.getPokemon(it2)
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it != null) {
                    var pokemonsList = ArrayList<PokemonInfo>()
                    for (pokemon in it) {
                        val subscribe = pokemon?.subscribe(Consumer { it1 ->
                            it1
                            pokemonsList.add(it1)
                        })
                    }
                    db.pokemonDao().insertPokemon(pokemonsList)
                }
            }, {
                Log.d("TEST_OF_LOADING_DATA", "Failure: " + it.message)
            })
        compositeDisposable.add(disposable)
    }

    class PokemonsViewModelFactory constructor(
        val app: Application
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass != PokemonsViewModel::class.java) {
                throw IllegalArgumentException("Unknown ViewModel class")
            }
            return PokemonsViewModel(
                app
            ) as T
        }
    }
}