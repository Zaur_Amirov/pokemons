package com.pokemons.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.pokemons.viewmodels.PokemonsViewModel
import com.pokemons.R
import com.pokemons.activities.MainActivity
import com.pokemons.adapters.PokemonAdapter
import com.pokemons.pojo.PokemonInfo
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    private lateinit var viewModel: PokemonsViewModel

    companion object {
        fun newInstance(): MainFragment {
            val fragment = MainFragment()
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { activity ->

            val adapter = PokemonAdapter(activity)
            adapter.onPokemonClickListener = object : PokemonAdapter.OnPokemonClickListener {
                override fun onPokemonClick(pokemonInfo: PokemonInfo) {
                    val activity = getActivity() as MainActivity?
                    activity?.onPokemonClick(pokemonInfo)
                }
            }
            rvPokemons.adapter = adapter

            viewModel = ViewModelProvider(
                this,
                PokemonsViewModel.PokemonsViewModelFactory(activity.application)
            ).get(
                PokemonsViewModel::class.java
            )

            viewModel.getPokemonList.observe(this, Observer {
                adapter.pokemonList = it
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
}