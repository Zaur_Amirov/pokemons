package com.pokemons.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pokemons.R
import com.pokemons.pojo.PokemonInfo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details.*

class PokemonDetailsFragment : NavigationChildFragment() {

    companion object {

        private const val NAME = "pokemon.details.name"
        private const val WEIGHT = "pokemon.details.weight"
        private const val HEIGHT = "pokemon.details.height"
        private const val BASE_EXPERIENCE = "pokemon.details.base_experience"

        private const val FRONT_DEFAULT = "pokemon.details.front_default"
        private const val FRONT_SHINY = "pokemon.details._front_shiny"
        private const val BACK_DEFAULT = "pokemon.details.back_default"
        private const val BACK_SHINY = "pokemon.details.back_shiny"

        fun newInstance(pokemonInfo: PokemonInfo): PokemonDetailsFragment {
            val fragment = PokemonDetailsFragment()
            var bundle = Bundle()
            bundle.putString(NAME, pokemonInfo.name)
            pokemonInfo.weight?.let { bundle.putInt(WEIGHT, it) }
            pokemonInfo.baseExperience?.let { bundle.putInt(HEIGHT, it) }
            pokemonInfo.height?.let { bundle.putInt(BASE_EXPERIENCE, it) }
            bundle.putString(FRONT_DEFAULT, pokemonInfo.sprites?.frontDefault)
            bundle.putString(FRONT_SHINY, pokemonInfo.sprites?.frontShiny)
            bundle.putString(BACK_DEFAULT, pokemonInfo.sprites?.backDefault)
            bundle.putString(BACK_SHINY, pokemonInfo.sprites?.backShiny)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get().load(arguments?.getString(BACK_DEFAULT)).into(ivBackDefault)
        Picasso.get().load(arguments?.getString(BACK_SHINY)).into(ivBackShiny)
        Picasso.get().load(arguments?.getString(FRONT_DEFAULT)).into(ivFrontDefault)
        Picasso.get().load(arguments?.getString(FRONT_SHINY)).into(ivFrontShiny)

        val nameTemplate = activity?.resources?.getString(R.string.name_template)
        val baseExperienceTemplate = activity?.resources?.getString(R.string.base_experience_template)
        val weightTemplate = activity?.resources?.getString(R.string.weight_template)
        val heightTemplate = activity?.resources?.getString(R.string.height_template)

        arguments?.getString(NAME)?.let { tvName.text =
            nameTemplate?.let { it1 -> String.format(it1, it) }
        }
        arguments?.getInt(WEIGHT)?.let { tvWeight.text =
            weightTemplate?.let { it1 -> String.format(it1, it) }
        }
        arguments?.getInt(HEIGHT)?.let { tvHeight.text =
            heightTemplate?.let { it1 -> String.format(it1, it) }
        }
        arguments?.getInt(BASE_EXPERIENCE)?.let { tvBaseExperience.text =
            baseExperienceTemplate?.let { it1 -> String.format(it1, it) }
        }
    }

    override fun onCreateChildView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }
}