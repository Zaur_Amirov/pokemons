package com.pokemons.fragments

interface OnBackChildFragmentEventListener {
    fun onBackChildFragment()
}